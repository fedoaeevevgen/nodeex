const express = require('express');
const bodyParser = require('body-parser');
var http = require('http');
var compression = require('compression');
var multer = require('multer');

var path = require('path');
var config = require('./libs/config');
const cookieParser = require('cookie-parser');//не факт что нужен из конечной сборки возможно надо удалить 
var MongoClient = require('mongodb').MongoClient;
var db;
MongoClient.connect('mongodb://127.0.0.1/superblog', function (err, database) {
if (err) {
return console.log(err);
}
db = database;
const app = express();



app.use(compression());

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
}));
app.use(multer(
    {
        dest: path.join(__dirname, 'public/uploads'),
        limits: {
            fieldNameSize: 999999999,
            fieldSize: 999999999
        }
    }
).any());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});
app.use('/files', express.static('../files'));

//************************* Routes ***********************************
require('./routes')(app,database);
//************************* 404 ***********************************
app.use(function(req, res){
    res.locals.metatitle = '404 Ничего не найдено';
    res.locals.pagenoindex = 'yes';
    res.send('404 ошибка');
});
//************************* Запуск сервера ***********************************
var httpServer = http.createServer(app);
function onListening(){
    console.log('Listening on port ', config.port);
}
httpServer.on('listening', onListening);
httpServer.listen(config.port, '127.0.0.1');

});
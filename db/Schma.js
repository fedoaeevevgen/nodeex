const mongoose = require('mongoose');
const crypto = require('crypto');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{
        type:String,
        unique:true,
        required:true
    }
},{
    collection: "User",
    versionKey: false
});

userSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    
    next();
});

module.exports = mongoose.model('User', userSchema);